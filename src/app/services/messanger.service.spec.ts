/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MessangerService } from './messanger.service';

describe('Service: Messanger', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MessangerService]
    });
  });

  it('should ...', inject([MessangerService], (service: MessangerService) => {
    expect(service).toBeTruthy();
  }));
});
