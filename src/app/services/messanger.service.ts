import { Injectable } from '@angular/core';
import { API_BASE_URL, Message } from '../constants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessangerService {

  constructor(private http: HttpClient) {

  }

  get() {
    return this.http.get<Message[]>(API_BASE_URL + 'main');
  }

  filter(search: string){
    return this.http.post<Message[]>(API_BASE_URL + 'ems/filter', search);
  }
}


