import { Injectable } from '@angular/core';
import { API_BASE_URL, Message } from '../constants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {
  }
  get() {
    return this.http.get<Message[]>(API_BASE_URL + 'user');
  }
  filter(search: string) {
    return this.http.post<Message[]>(API_BASE_URL + 'user/filter', search);
  }
}
