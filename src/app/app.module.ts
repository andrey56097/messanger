import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { MessangerComponent } from './messanger/messanger.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpRequestInterceptor } from './http-intercepter';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MessangerComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [            
    // Http Interceptor(s) -  adds with Client Credentials
    [
        { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true }
    ],
],
  bootstrap: [AppComponent]
})
export class AppModule { }
