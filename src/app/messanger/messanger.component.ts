import { Component, OnInit } from '@angular/core';
import { API_BASE_URL, Message } from '../constants';
import { HttpClient } from '@angular/common/http';
import { MessangerService } from '../services/messanger.service';

@Component({
  selector: 'app-messanger',
  templateUrl: './messanger.component.html',
  styleUrls: ['./messanger.component.scss']
})
export class MessangerComponent implements OnInit {

  title = 'messanger';
  msgs: Message[] = [];

  newMsg: Message = new Message();

  constructor(private messangerService: MessangerService) {

  }

  getMsgs() {
    this.messangerService.get().subscribe(data => this.msgs = data);
  }

  ngOnInit() {
  }

}
