import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_URL } from '../constants';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login: string = 'u';
  pass: string = 'p';

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  loginMethod(){
    var data  = new FormData();
    data.set('username', this.login);
    data.set('password', this.pass);

    this.http.post(API_BASE_URL + 'login', data, {responseType: 'text', withCredentials: true}).subscribe(()=>{

    });
  }

}
