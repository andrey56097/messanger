export const API_BASE_URL = 'http://localhost:8080/';

export class Message{
  id: number;
  text: string;
  tag: string;
}